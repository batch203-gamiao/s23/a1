// console.log("Hello World");

let trainer = {
	name: "Ask Ketchum",
	age: 10,	
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(target){
		var item = target;
		var index = this.pokemon.indexOf(item);

		if (target == this.pokemon[index]){
			console.log(item + "! I choose you!");
		}
	}
}

console.log(trainer);

// Dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

// Square bracket notation
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Talk method
console.log("Result of talk method");
trainer.talk("Pikachu");

